import template from './line-chart.template.html'

class controller {
    constructor(CurrenciesAPI, CurrencyToFavService) {
        Object.assign(this, {
            CurrenciesAPI,
            CurrencyToFavService,

            datasetOverride: [{ yAxisID: 'y-axis-1' }],
            options: {
                scales: {
                    yAxes: [
                        {
                            id: 'y-axis-1',
                            type: 'linear',
                            display: true,
                            position: 'left'
                        }
                    ]
                }
            }
        });
    }

    getData() {
        return this.CurrenciesAPI.getFromCache('currencies-by-code-payload');
    }

    displayExchangeRates(code, dates) {
        return this.CurrenciesAPI.getByCodeAndDates
    }
}

export default {
    template,
    controller
}
