import template from './bar-chart.template.html'

class controller {
    constructor(CurrenciesAPI, CurrencyToFavService) {
        Object.assign(this, {

            CurrenciesAPI,
            CurrencyToFavService,


            labels: [
                '2006',
                '2007',
                '2008',
                '2009',
                '2010',
                '2011',
                '2012'
            ],

            series: ['Series A', 'Series B'],
            data: [
                [65, 59, 80, 81, 56, 55, 40],
                [28, 48, 40, 19, 86, 27, 90]
            ]
        })
    }

    getData() {
        return this.CurrenciesAPI.getFromCache('currencies-by-code-payload');
    }

    displayExchangeRates(code, dates) {
        return this.CurrenciesAPI.getByCodeAndDates

    }
}

export default {
    template,
    controller
}
