import angular from 'angular'
import angularChart from 'angular-chart.js'

import chartsViewComponent from "./charts-view.component";
import lineChartComponent from './line-chart/line-chart.component';
import currencyPickerComponent from './currency-picker/currency-picker.component';
import chartsViewNavigationComponent from './charts-view-navigation/charts-view-navigation.component';
import barChartComponent from './bar-chart/bar-chart.component';

export default angular
        .module('ChartsView', [angularChart])
        .component('chartsView', chartsViewComponent)
        .component('lineChart', lineChartComponent)
        .component('currencyPicker', currencyPickerComponent)
        .component('chartsViewNavigation', chartsViewNavigationComponent)
        .component('barChart', barChartComponent)
