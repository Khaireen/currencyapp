import moment from 'moment'
import template from './charts-view.template.html'

class controller {
    constructor(CurrencyToFavService, CurrenciesAPI) {
        const { from, to } = CurrencyToFavService.getExchangeRatesDates();

        Object.assign(this, {
            CurrencyToFavService,
            CurrenciesAPI,

            dates: {
                from,
                to
            },
            currencyCode: ''
        });
    }

    _convertToString(date) {
        return moment(date).format('YYYY-MM-DD');
    }

    getCode(code) {
        return this.currencyCode = code;
    }

    acceptButtonHandler(from, to) {
        return this
            .CurrenciesAPI
            .getByCodeAndDates(
                this.currencyCode,
                this._convertToString(from),
                this._convertToString(to)
            )
            .then(response => {
                const { rates, currency } = response;

                return {
                    labels: rates.map(item => item.effectiveDate),
                    series: [currency],
                    data: [rates.map(item => item.mid)]
                };
            })
            .then(r => { console.log(r); return r; })
            .then(response => this.CurrenciesAPI.setCache('currencies-by-code-payload', response))
            .catch(console.error);
    }
}

export default {
    template,
    controller
}
