import template from './currency-picker.template.html'

class controller {
    constructor(CurrencyToFavService) {
        this.CurrencyToFavService = CurrencyToFavService;
        const options = this.CurrencyToFavService.get();
        this.options = options;
        this.selectedValue = null;
    }
}

export default {
    template,
    controller,
    bindings: {
        getValue: '&'
    }
}
