import moment from 'moment'

export default class CurrencyAppController {
    constructor(CurrenciesAPI, CurrencyToFavService) {
        this.CurrenciesAPI = CurrenciesAPI;
        this.CurrencyToFavService = CurrencyToFavService;
        this.collection = [];
    }

    _convertToString(date) {

        return moment(date)
            .format('YYYY-MM-DD');
    }

    submit(dateFrom, dateTo) {
        this.CurrenciesAPI
            .get(
                this._convertToString(dateFrom),
                this._convertToString(dateTo)
            )
            .then(response => this.collection = response)
            .catch(console.error);
    }

    addCurrencyToFavourites(currency, date) {
        return this.CurrencyToFavService
            .handler(...arguments);
    }

    isFavorite(currency) {
        return this.CurrencyToFavService.find(currency);
    }
}
