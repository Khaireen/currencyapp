function generateIdForDailyRate(data) {
    return data.map(dailyExchange => {
        const rates = dailyExchange
            .rates
            .map(
                rate => ({
                    ...rate,
                    id: `${dailyExchange.effectiveDate}/${rate.code}`
                })
            );

        return {
            ...dailyExchange,
            rates
        };
    })
}

function CurrenciesAPI($http, $cacheFactory) {
    this._$http = $http;
    this.cache = $cacheFactory('currency-data');
    this.setCache = this.setCache.bind(this);
}

Object.assign(CurrenciesAPI.prototype, {
    get(dateFrom, dateTo, format = 'json') {
        const config = {
            params: { format }
        };

        return this
            ._$http
            .get(
                `http://api.nbp.pl/api/exchangerates/tables/A/${dateFrom}/${dateTo}`,
                config
            )
            .then(response => response.data)
            .then(generateIdForDailyRate);
    },

    getByCodeAndDates(code, dateFrom, dateTo) {
        return this
            ._$http
            .get(
                `http://api.nbp.pl/api/exchangerates/rates/A/${code}/${dateFrom}/${dateTo}`
            )
            .then(response => response.data);
    },

    getFromCache(cacheId) {
        return this.cache.get(cacheId);
    },

    setCache(cacheId, value) {
        return this.cache.put(...arguments);
    }
})

export default CurrenciesAPI
