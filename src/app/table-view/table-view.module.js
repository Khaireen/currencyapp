import angular from 'angular'

import tableViewComponent from './table-view.component'
import tableViewNavigationComponent from './table-view-navigation/table-view-navigation.component';
import favouriteCurrencyContainerComponent from './favourite-currencies/favourite-currency-container/favourite-currency-container.component';
import currenciesListTableComponent from './currencies-list/currencies-list-table/currencies-list-table.component';


export default angular
        .module('TableView', [])
        .component('tableView', tableViewComponent)
        .component('tableViewNavigation', tableViewNavigationComponent)
        .component('favouriteCurrencyContainer', favouriteCurrencyContainerComponent)
        .component('currenciesListTable', currenciesListTableComponent)