import template from './favourite-currency-container.template.html'

class controller {
    constructor(CurrencyToFavService) {
        this.CurrencyToFavService = CurrencyToFavService;
    }

    getFavs() {
        return this.CurrencyToFavService.get();
    }

    clear() {
        return this.CurrencyToFavService.removeAll();
    }

    remove(currency) {
        return this.CurrencyToFavService
            .handler(currency);
    }
}

export default {
    template,
    controller,
    bindings: {
        currencies: '<'
    }
}


