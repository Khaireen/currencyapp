import moment from 'moment'
import template from './table-view.template.html'

class controller {
    constructor(CurrencyToFavService, CurrenciesAPI) {
        Object.assign(this, {
            CurrencyToFavService,
            CurrenciesAPI
        });
    }

    _convertToString(date) {
        return moment(date).format('YYYY-MM-DD');
    }

    acceptButtonHandler(dateFrom, dateTo) {
        this.CurrencyToFavService
            .setExchangeRatesDates(...arguments);

        return this.CurrenciesAPI
            .get(
                this._convertToString(dateFrom),
                this._convertToString(dateTo)
            ).then(response => this.CurrenciesAPI.setCache('currency-by-date-payload', response))
            .catch(console.error);
    }

    isFavorite(currency) {
        return this.CurrencyToFavService.find(currency);
    }

    addCurrencyToFavourites(currency, date) {
        return this.CurrencyToFavService
            .handler(...arguments);
    }
}

export default {
    template,
    controller
}

