import template from './currencies-list-table.template.html'

class controller {
    constructor() { }
}

export default {
    controller,
    template,
    bindings: {
        currenciesCollection: '<',
        addCurrencyToFavourites: '&',
        isFavorite: '&'
    }
}