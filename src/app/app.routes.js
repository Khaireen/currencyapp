export default function Routes($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');

    const homeViewState = {
        name: 'homeView',
        url: '/home',
        component: 'homeView'
    };

    const tableViewState = {
        name: 'tableView',
        url: '/tables',
        component: 'tableView'
    };

    const chartsViewState = {
        name: 'chartsView',
        url: '/charts',
        component: 'chartsView'
    }

    $stateProvider
        .state(homeViewState)
        .state(tableViewState)
        .state(chartsViewState);
}

Routes.$inject = ['$stateProvider', '$urlRouterProvider'];
