import template from './date-picker-input.template.html'

export default {
    template,
    bindings: {
        dates: '<',
        acceptButtonHandler: '&'
    }
}


