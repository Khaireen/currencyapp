import template from './accept-date-button.template.html'

export default {
    template,
    bindings: {
        dates: '<',
        acceptButtonHandler: '&'
    }
}
