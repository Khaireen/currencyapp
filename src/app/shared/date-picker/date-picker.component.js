import moment from 'moment'
import template from './date-picker.template.html'

class controller {
    constructor() { }

    $onInit() {
        this.dates = {
            from: this._convertDateToNativeObject(this.dateFrom),
            to: this._convertDateToNativeObject(this.dateTo)
        };
    }

    _convertDateToNativeObject(date = new Date()) {
        return moment(date).toDate();
    }
}

export default {
    template,
    controller,
    bindings: {
        acceptButtonHandler: '&',
        dateFrom: '<',
        dateTo: '<'
    }
}

