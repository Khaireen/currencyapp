import angular from 'angular'

import homeViewComponent from './home-view.component'
import homeViewNavigationComponent from './home-view-navigation/home-view-navigation.component'
import homeViewButtonComponent from './home-view-button/home-view-button.component'

export default angular
    .module('HomeView', [])
    .component('homeView', homeViewComponent)
    .component('homeViewNavigation', homeViewNavigationComponent)
    .component('homeViewButton', homeViewButtonComponent)
