import angular from 'angular'
import uiRouter from 'angular-ui-router'

import CurrencyAppController from './app.controller'
import Routes from './app.routes'
import CurrenciesAPI from './api/currencies-api'
import CurrencyToFavService from './services/currency-to-fav.service'
import datePickerComponent from './shared/date-picker/date-picker.component'
import datePickerInputComponent from './shared/date-picker/date-picker-input/date-picker-input.component'
import acceptDateButtonComponent from './shared/date-picker/date-picker-input/date-picker-button/accept-date-button.component'
import HomeViewModule from './home-view/home-view.module'
import TableViewModule from './table-view/table-view.module'
import ChartsViewModule from './charts-view/charts-view.module';

const EXTERNAL_DEPS = [
    uiRouter
]

const OWN_DEPS = [
    TableViewModule.name,
    HomeViewModule.name,
    ChartsViewModule.name
]

const DEPS = [...EXTERNAL_DEPS, ...OWN_DEPS]

export default angular
    .module('CurrencyApp', DEPS)
    .controller('CurrencyAppController', CurrencyAppController)
    .service('CurrenciesAPI', CurrenciesAPI)
    .service('CurrencyToFavService', CurrencyToFavService)
    .component('datePicker', datePickerComponent)
    .component('datePickerInput', datePickerInputComponent)
    .component('acceptDateButton', acceptDateButtonComponent)
    .config(Routes)