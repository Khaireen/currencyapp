export default class CurrencyToFavService {
    constructor() {
        this.currencies = [];
        this.exchangeRatesDates = {
            from: null,
            to: null
        }
    }

    add(currency) {
        return this.currencies.push(currency);
    }

    remove(index) {
        return this.currencies.splice(index, 1);
    }

    removeAll() {
        return this.currencies = [];
    }

    handler(currency, date) {
        const foundIndex = this.get()
            .findIndex(item => item.id === currency.id);

        if (foundIndex > -1) {
            return this.remove(foundIndex);
        }

        return this.add(Object.assign(currency, { date }));
    }

    find(currency) {
        return this.get().find(item => item.id === currency.id);
    }

    get() {
        return this.currencies;
    }

    setExchangeRatesDates(from, to) {
        return this.exchangeRatesDates = { from, to };
    }

    getExchangeRatesDates() {
        return this.exchangeRatesDates;
    }
}
