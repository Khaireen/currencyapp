const {
    plugin
} = require( 'webpack-partial' );

const BrowserSyncPlugin = require( 'browser-sync-webpack-plugin' );
const spa               = require( 'browser-sync-spa' );

const options = {
    browserSyncPlugin: [
        {
            host: 'localhost',
            port: 3000,
            proxy: {
                target: 'http://localhost:3100/',
                ws: true
            },
            logLevel: 'debug',
            logPrefix: 'BROWSER SYNC LOGGING',
            open: false,
            plugins: [
                spa({
                    selector: '[ng-app]',
                    history: { index: 'index.html' }
                })
            ]
        },
        { reload: false }
    ]
};

const browserSyncPlugin = () => plugin(new BrowserSyncPlugin( ...options.browserSyncPlugin ));

module.exports = {
    browserSyncPlugin
};
