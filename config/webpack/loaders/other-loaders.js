const {
    loader
} = require( 'webpack-partial' );

const fontLoader = options => loader({
    test: /\.(woff|woff2|eot|ttf|svg)$/,
    use: {
        loader: 'url-loader',
        options
    }
});

const imageLoader = options => loader({
    test: /\.(png|jpg|jpeg)$/i,
    use: {
        loader: 'file-loader',
        options
    }
});

const htmlLoader = options => loader({
    test: /\.html$/,
    use: {
        loader: 'html-loader',
        options
    }
});

const linter = options => loader({
    test: /\.js$/,
    exclude: /(node_modules|src\/(legacy|assets.*))/,
    use: {
        loader: 'eslint-loader',
        options
    },
    enforce: 'pre'
});

module.exports = {
    imageLoader,
    fontLoader,
    htmlLoader,
    linter
};