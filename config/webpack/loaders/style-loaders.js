const {
    prop
} = require( 'ramda' );

const {
    loader
} = require( 'webpack-partial' );

const css = loaderConfig => loader({
    test: /\.css$/,
    use: [
        {
            loader: 'style-loader',
            options: prop('styleLoader', loaderConfig)
        },
        {
            loader: 'css-loader',
            options: prop('cssLoader', loaderConfig)
        }
    ]
});

module.exports = { css };