# CurrencyApp

This app was created solely as an introduction to AngularJS and was meant to be my reconnaisance by fire. 

Feel free to leave comments regarding code review. 

TODO

1. Styling:
    - navigation buttons 
    - table view
    - charts-view input and select section

2. UI
    - Charts navigation button should be disabled until tables are loaded and favourite currencies are selected.
    - Need more instruciotn for users, explaining application functionalities and purpose.

3. Development (as a continuation of my learning process) 
    - implementation of Redux
    - implementation of Angular Material


